<?
/**
 * @param $dir
 * @return bool
 * Удаляет папку и ее содержимое
 */
function delFolder($dir) {
    $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? delFolder("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}

/**
 * @param $arArray
 */
function ppr($arArray) {
    echo "<pre>";
    print_r($arArray);
    echo "</pre>";
}