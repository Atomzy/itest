<?
/**
 * Class warehouses
 */
Class warehouses extends CModule {
    /**
     * warehouses constructor.
     */
    public function __construct() {
        /*объявлем переменную и подключаем файл версии*/
        $arModuleVersion = Array();
        include("version.php");

        /*заполняем свойства*/
        $this->MODULE_ID = "warehouses";
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = "Остатки на складах";
        $this->MODULE_DESCRIPTION = "Модуль позволяющий выводить информацию об остатке на складах";

        $this->COMPONTNRS_REPOSITORY = "kvo";
    }

    /**
     * Копирование компонентов модуля
     */
    public function InstallComponents() {
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/{$this->MODULE_ID}/install/components/{$this->COMPONTNRS_REPOSITORY}",
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/{$this->COMPONTNRS_REPOSITORY}",
            true,
            true
        );
    }

    /*
     * Удаление компонентов модуля
     * */
    public function UnInstallComponents() {
        delFolder($_SERVER["DOCUMENT_ROOT"]."/bitrix/components/{$this->COMPONTNRS_REPOSITORY}");
    }

    /**
     * Включение модуля
     */
    public function DoInstall() {
        Global $DOCUMENT_ROOT, $APPLICATION;

        /*Копируем компоненты модуля в общую папку компонентов*/
        $this->InstallComponents();

        /*Включаем модуль*/
        RegisterModule($this->MODULE_ID);

        /*После включения модуля, подключаем файл с подтверждением завершения установки*/
        $APPLICATION->IncludeAdminFile("Модуль warehouses установлен", $DOCUMENT_ROOT."/bitrix/modules/warehouses/install/step.php");
    }

    /**
     * Отключение моудля
     */
    public function DoUninstall() {
        Global $DOCUMENT_ROOT, $APPLICATION;

        /*Отключаем модуль*/
        UnRegisterModule($this->MODULE_ID);

        /*Удаляем компоненты модуля*/
        $this->UnInstallComponents();

        /*После выключения модуля, подключаем файл с подтверждением завершения удаления*/
        $APPLICATION->IncludeAdminFile("Модуль warehouses удален", $DOCUMENT_ROOT."/bitrix/modules/warehouses/install/unstep.php");
    }
}