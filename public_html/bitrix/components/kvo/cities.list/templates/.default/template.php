<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script src="/bitrix/js/jquery.cookie.js"></script>
<div id="map" style="display: none;"></div>

Ваш город: <span class="my-city"></span>, <a href="#" data-toggle="modal" data-target="#myModal">выбрать другой</a>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Выбрать город</h4>
            </div>
            <div class="modal-body">
                <div class="region">
                    <a href="#" class="main">Москва</a>
                    <a href="#">Подольск</a>
                    <a href="#">Пушкино</a>
                </div>
                <div class="region">
                    <a href="#" class="main">Ульяновск</a>
                    <a href="#">Дмитровград </a>
                    <a href="#">Сенгилей</a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>