$(function () {
    /*инициализируем яндекс карты*/
    ymaps.ready(init);

    /*обробатываем*/
    function init() {
        /*получаем геолокацию*/
        var geo = ymaps.geolocation;

        /*
        проверяем заполнены ли куки, показываем их, иначе показываем город, определеный по ip
        */
        if($.cookie('city')) {
            $('.my-city').text($.cookie('city'));
        } else {
            $('.my-city').text(geo.city);
        }

        /*заполняем куки по клику города в модальном окне*/
        $(".region a").click(function () {
            $.cookie('city', $(this).text(), {path: '/'});
            location.reload();
        });
    }
});